let posicionPrincipal = window.pageYOffset;
window.onscroll = function(){
    let desplazamiento = window.pageYOffset;
    if(posicionPrincipal >= desplazamiento){
        document.getElementById("piePagina").style.bottom = "0";
        document.getElementById("piePagina").style.transition = "0.5s ease";
        document.getElementById("topBarCel").style.top = "0";
        document.getElementById("topBarCel").style.transition = "0.5s ease";
    }else{
        document.getElementById("piePagina").style.bottom = "-10%";
        document.getElementById("piePagina").style.transition = "0.5s ease";
        document.getElementById("topBarCel").style.top = "-14%";
        document.getElementById("topBarCel").style.transition = "0.5s ease";
    }
    posicionPrincipal = desplazamiento;
}